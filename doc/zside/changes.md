# Explanation of how zcash code was transplanted into the sidechain template to produce zside

[[_TOC_]]
## Introduction
This document describes most changes that were made to the sidechain template to
produce zside. Most of it was just copy pasting code from zcash, but in a few
places there are deviations.

The most substantial deviations from just copy pasting are described in the
Anchors section.
## `class CTransaction` and `struct CMutableTransaction`
### Copy paste
* All fields that zcash transactions have are added:
```c++
// New zcash fields
const uint32_t nExpiryHeight;
const CAmount valueBalance;
const std::vector<SpendDescription> vShieldedSpend;
const std::vector<OutputDescription> vShieldedOutput;
const std::vector<JSDescription> vJoinSplit;
const uint256 joinSplitPubKey;
const joinsplit_sig_t joinSplitSig = {{0}};
const binding_sig_t bindingSig = {{0}};
```
Definition and implementation of `SpendDescription`, `OutputDescription`, and
`JSDescription` classes are just copy pasted;

* `GetShieldedValueIn()` method is added.
### Serialization
Serialization in sidechain doesn't work exactly like in zcash, so the code is
not just copy pasted here.

In zcash there is a `SerializationOp(Stream& s, Operation ser_action)` method in
both `class CTransaction` and `struct CMutableTransaction` that implements both
serialization and deserialization. It is done by using `READWRITE` macros.

For `CTransaction` in zside (like in the template) there is a `Serialize(Stream& s)`
method and a deserialization constructor.
```c++
template <typename Stream>
inline void Serialize(Stream& s) const {
    SerializeTransaction(*this, s);
}

/** This deserializing constructor is provided instead of an Unserialize method.
    *  Unserialize is not possible, since it would require overwriting const fields. */
template <typename Stream>
CTransaction(deserialize_type, Stream& s) : CTransaction(CMutableTransaction(deserialize, s)) {}
```

For CMutableTransaction struct there are both an Unserialize method and a
deserialization constructor.
```c++
template <typename Stream>
inline void Serialize(Stream& s) const {
    SerializeTransaction(*this, s);
}

template <typename Stream>
inline void Unserialize(Stream& s) {
    UnserializeTransaction(*this, s);
}

template <typename Stream>
CMutableTransaction(deserialize_type, Stream& s) {
    Unserialize(s);
}
```
Ultimately `SerializeTransaction(const TxType& tx, Stream& s)` and
`UnserializeTransaction(TxType& tx, Stream& s)` are used for serialization and
deserialization respectively. There are a few differences in the implementation
as well.

* In these methods we don't use `READWRITE` (unlike in zcash);
* Instead we use `>>` and `<<` operators to read from and write to the Stream&;
* There is a special `unsigned char flags` byte in a serialized transaction that
  is used to toggle witnesses support in the original sidechain template;
* In zside we use it to distinguish purely transparent transaction and
  transactions with shielded inputs/outputs. There is this if statement at the end of the implementation:
  ```c++
  // old sidechain template code
  if (flags & ZCASH_FLAG) {
      // new zcash serialization/deserialization code
  }
  ```
* bools `isOverwinterV3` and `isSaplingV4` are constants set to `true`;
* Otherwise zside code for de/serializing zcash fields in transactions is copy
  pasted. Every `s << field;` or `s >> field;` call in zside corresponds to a
  `READWRITE(field)` call in zcash.
## Anchors
### `ThreadNotifyWallets()` in zcash

There is a `ThreadNotifyWallets(CBlockIndex *pindexLastTip)` function in
validationinterface.cpp. It runs an infinite loop, and it waits for one second
at the beginning of every iteration:

```c++
void ThreadNotifyWallets(CBlockIndex *pindexLastTip)
{
    // ...
    while (true) {
        // Run the notifier on an integer second in the steady clock.
        auto now = std::chrono::steady_clock::now().time_since_epoch();
        auto nextFire = std::chrono::duration_cast<std::chrono::seconds>(
            now + std::chrono::seconds(1));
        std::this_thread::sleep_until(
            std::chrono::time_point<std::chrono::steady_clock>(nextFire));

        boost::this_thread::interruption_point();

        // ...
    }

}
```

In init.cpp in zcash it is setup to run in a separate thread:

```c++
// Start the thread that notifies listeners of transactions that have been
// recently added to the mempool, or have been added to or removed from the
// chain. We perform this before step 10 (import blocks) so that the
// original value of chainActive.Tip(), which corresponds with the wallet's
// view of the chaintip, is passed to ThreadNotifyWallets before the chain
// tip changes again.
{
    CBlockIndex *pindexLastTip;
    {
        LOCK(cs_main);
        pindexLastTip = chainActive.Tip();
    }
    boost::function<void()> threadnotifywallets = boost::bind(&ThreadNotifyWallets, pindexLastTip);
    threadGroup.create_thread(
        boost::bind(&TraceThread<boost::function<void()>>, "txnotify", threadnotifywallets)
    );
}
```

This function collects connected blocks, conflicted transactions, and
transactions recently added to mempool:

```c++
//
// Collect all the state we require
//

// The common ancestor between the last chain tip we notified and the
// current chain tip.
const CBlockIndex *pindexFork;
// The stack of blocks we will notify as having been connected.
// Pushed in reverse, popped in order.
std::vector<CachedBlockData> blockStack;
// Transactions that have been recently conflicted out of the mempool.
std::pair<std::map<CBlockIndex*, std::list<CTransaction>>, uint64_t> recentlyConflicted;
// Transactions that have been recently added to the mempool.
std::pair<std::vector<CTransaction>, uint64_t> recentlyAdded;

{
    LOCK(cs_main);

    // Figure out the path from the last block we notified to the
    // current chain tip.
    CBlockIndex *pindex = chainActive.Tip();
    pindexFork = chainActive.FindFork(pindexLastTip);

    // Fetch recently-conflicted transactions. These will include any
    // block that has been connected since the last cycle, but we only
    // notify for the conflicts created by the current active chain.
    recentlyConflicted = DrainRecentlyConflicted();

    // Get the Sapling commitment tree as of the start of this block.
    // We can get this from the `hashFinalSaplingRoot` of the last block
    // However, this is only reliable if the last block was on or after
    // the Sapling activation height. Otherwise, the last anchor was the
    // empty root.
    SaplingMerkleTree oldSaplingTree;
    if (chainParams.GetConsensus().NetworkUpgradeActive(
        pindex->pprev->nHeight, Consensus::UPGRADE_SAPLING)) {
        assert(pcoinsTip->GetSaplingAnchorAt(
            pindex->pprev->hashFinalSaplingRoot, oldSaplingTree));
    } else {
        assert(pcoinsTip->GetSaplingAnchorAt(SaplingMerkleTree::empty_root(), oldSaplingTree));
    }

    blockStack.emplace_back(
        pindex,
        std::make_pair(oldSproutTree, oldSaplingTree),
        recentlyConflicted.first.at(pindex));

    pindex = pindex->pprev;
}

recentlyAdded = mempool.DrainRecentlyAdded();
```
    
Then we notify block disconnects:
```c++
// Notify block disconnects
while (pindexLastTip && pindexLastTip != pindexFork) {
    // Read block from disk.
    CBlock block;
    if (!ReadBlockFromDisk(block, pindexLastTip, chainParams.GetConsensus())) {
        LogPrintf("*** %s\n", "Failed to read block while notifying wallets of block disconnects");
        uiInterface.ThreadSafeMessageBox(
            _("Error: A fatal internal error occurred, see debug.log for details"),
            "", CClientUIInterface::MSG_ERROR);
        StartShutdown();
    }

    // Let wallets know transactions went from 1-confirmed to
    // 0-confirmed or conflicted:
    for (const CTransaction &tx : block.vtx) {
        SyncWithWallets(tx, NULL);
    }
    LogPrintf("Disconnect ChainTip\n");
    // Update cached incremental witnesses
    GetMainSignals().ChainTip(pindexLastTip, &block, boost::none);

    // On to the next block!
    pindexLastTip = pindexLastTip->pprev;
}
```

block connections:
```c++
// Notify block connections
while (!blockStack.empty()) {
    auto blockData = blockStack.back();
    blockStack.pop_back();

    // Read block from disk.
    CBlock block;
    if (!ReadBlockFromDisk(block, blockData.pindex, chainParams.GetConsensus())) {
        LogPrintf("*** %s\n", "Failed to read block while notifying wallets of block connects");
        uiInterface.ThreadSafeMessageBox(
            _("Error: A fatal internal error occurred, see debug.log for details"),
            "", CClientUIInterface::MSG_ERROR);
        StartShutdown();
    }

    // Tell wallet about transactions that went from mempool
    // to conflicted:
    for (const CTransaction &tx : blockData.txConflicted) {
        SyncWithWallets(tx, NULL);
    }
    // ... and about transactions that got confirmed:
    for (const CTransaction &tx : block.vtx) {
        SyncWithWallets(tx, &block);
    }

    // Update cached incremental witnesses
    GetMainSignals().ChainTip(blockData.pindex, &block, blockData.oldTrees);

    // This block is done!
    pindexLastTip = blockData.pindex;
}
```

and transactions recently added to mempool:
```c++
// Notify transactions in the mempool
for (auto tx : recentlyAdded.first) {
    try {
        SyncWithWallets(tx, NULL);
    } catch (const boost::thread_interrupted&) {
        throw;
    } catch (const std::exception& e) {
        PrintExceptionContinue(&e, "ThreadNotifyWallets()");
    } catch (...) {
        PrintExceptionContinue(NULL, "ThreadNotifyWallets()");
    }
}
```

Notification means calling `SyncWithWallets` for every transaction in a block
and `ChainTip` on the block in case of block connection/disconnection and just
`SyncWithWallets` for every transaction in case of new mempool transactions.

In conclusion, this function runs an infinite loop in a separate thread and it
sleeps for one second at the start of every iteration. Then it collects all
connected blocks, disconnected blocks, and all transactions that were added to
mempool over that one second.

Then we notify all disconnected blocks and all connected blocks in the current
active chain. And after that we go through all transactions recently added to
mempool and call `SyncWithWallets` on each.

Note that `SyncWithWallets` is just a wrapper around `SyncTransaction` in zcash:

```c++
void SyncWithWallets(const CTransaction &tx, const CBlock *pblock) {
    g_signals.SyncTransaction(tx, pblock);
}
```

And also note that `g_signals.SyncTransaction` and `g_signals.ChainTip` methods
will run `CWallet::SyncTransaction` and `CWallet::ChainTip`.
    
### `CWallet::BlockConnected`, `CWallet::BlockDisconnected`, and `CWallet::TransactionAddedToMempool` in zside
Completely replicating `ThreadNotifyWallets` code in the sidechain template was
very awkward and it didn't actually work. Figuring this out was the most
difficult part of the project.

There are `CWallet::BlockConnected`, `CWallet::BlockDisconnected`, and
`CWallet::TransactionAddedToMempool` methods in sidechain template. And there
are no such methods in zcash.

If we loook at implementations of `BlockConnected` and `BlockDisconnected`:
```c++
void CWallet::BlockConnected(const std::shared_ptr<const CBlock>& pblock, const CBlockIndex *pindex, const std::vector<CTransactionRef>& vtxConflicted) {
    LOCK2(cs_main, cs_wallet);

    for (const CTransactionRef& ptx : vtxConflicted) {
        SyncTransaction(ptx);
        TransactionRemovedFromMempool(ptx);
    }
    for (size_t i = 0; i < pblock->vtx.size(); i++) {
        SyncTransaction(pblock->vtx[i], pindex, i);
        TransactionRemovedFromMempool(pblock->vtx[i]);
    }

    m_last_block_processed = pindex;
}

void CWallet::BlockDisconnected(const std::shared_ptr<const CBlock>& pblock) {
    LOCK2(cs_main, cs_wallet);

    for (const CTransactionRef& ptx : pblock->vtx) {
        SyncTransaction(ptx);
    }
}
```

we can see that they almost do what we want. In `BlockConnected` we iterate over
all conflicted transactions and all transactions in the block and call
`SyncTransaction` on each. In `BlockDisconnected` we do the same just for block
transactions.

In zside we just need to add a call to `CWallet::ChainTip` in both
`BlockConnected` and `BlockDisconnected`:

```c++
void CWallet::BlockConnected(const std::shared_ptr<const CBlock>& pblock, const CBlockIndex *pindex, const std::vector<CTransactionRef>& vtxConflicted) {
    LOCK2(cs_main, cs_wallet);

    for (const CTransactionRef& ptx : vtxConflicted) {
        SyncTransaction(ptx);
        TransactionRemovedFromMempool(ptx);
    }
    for (size_t i = 0; i < pblock->vtx.size(); i++) {
        SyncTransaction(pblock->vtx[i], pindex, i);
        TransactionRemovedFromMempool(pblock->vtx[i]);
    }

    SaplingMerkleTree oldSaplingTree;
    if (pindex->pprev) {
        assert(pcoinsTip->GetSaplingAnchorAt(pindex->pprev->hashLightClientRoot, oldSaplingTree));
    } else {
        assert(pcoinsTip->GetSaplingAnchorAt(SaplingMerkleTree::empty_root(), oldSaplingTree));
    }
    ChainTip(pindex, pblock.get(), oldSaplingTree);

    m_last_block_processed = pindex;
}

void CWallet::BlockDisconnected(const std::shared_ptr<const CBlock>& pblock, const CBlockIndex *pindex) {
    LOCK2(cs_main, cs_wallet);

    for (const CTransactionRef& ptx : pblock->vtx) {
        SyncTransaction(ptx);
        // Update cached incremental witnesses
        // ChainTip(pindex, &*pblock, boost::none);
    }
    // Update cached incremental witnesses
    ChainTip(pindex, &*pblock, boost::none);
}
```

We handled block connections and disconnections. Next we need to handle
transactions being added to mempoool. For that we have
`CWallet::TransactionAddedToMempool` in the original sidechain template, and it
already has a call to `CWallet::SyncTransaction`:

```c++
void CWallet::TransactionAddedToMempool(const CTransactionRef& ptx) {
    LOCK2(cs_main, cs_wallet);
    SyncTransaction(ptx);

    auto it = mapWallet.find(ptx->GetHash());
    if (it != mapWallet.end()) {
        it->second.fInMempool = true;
    }
}
```

so we don't have to change anything here.

In zside after a block is connected or disconnected it is notified immediately,
and when a new transaction is added to mempool it is also notified immediately.

It might be possible to replicate zcash approach with `ThreadNotifyWallets` in
zside, but it would require rewriting all the code sidechain template code that
relies on `BlockConnected`/`BlockDisconnected`/`TransactionAddedToMempool`.

`CWallet::SyncTransaction` method in original sidechain looks like this:
```c++
void CWallet::SyncTransaction(const CTransactionRef& ptx, const CBlockIndex *pindex, int posInBlock) {
    const CTransaction& tx = *ptx;

    if (!AddToWalletIfInvolvingMe(ptx, pindex, posInBlock, true))
        return; // Not one of ours

    // If a transaction changes 'conflicted' state, that changes the balance
    // available of the outputs it spends. So force those to be
    // recomputed, also:
    for (const CTxIn& txin : tx.vin) {
        auto it = mapWallet.find(txin.prevout.hash);
        if (it != mapWallet.end()) {
            it->second.MarkDirty();
        }
    }
}
```

in zside we just add a cycle iterating over shielded inputs (`tx.vShieldedSpend`):
```c++
void CWallet::SyncTransaction(const CTransactionRef& ptx, const CBlockIndex *pindex, int posInBlock) {
    const CTransaction& tx = *ptx;

    if (!AddToWalletIfInvolvingMe(ptx, pindex, posInBlock, true)) {
        LogPrintf("Not one of ours\n");
        return; // Not one of ours
    }

    // If a transaction changes 'conflicted' state, that changes the balance
    // available of the outputs it spends. So force those to be
    // recomputed, also:
    for (const CTxIn& txin : tx.vin) {
        auto it = mapWallet.find(txin.prevout.hash);
        if (it != mapWallet.end()) {
            it->second.MarkDirty();
        }
    }

    for (const SpendDescription &spend : tx.vShieldedSpend) {
        uint256 nullifier = spend.nullifier;
        if (mapSaplingNullifiersToNotes.count(nullifier) &&
            mapWallet.count(mapSaplingNullifiersToNotes[nullifier].hash)) {
            mapWallet[mapSaplingNullifiersToNotes[nullifier].hash].MarkDirty();
        }
    }
}
```

`CWallet::ChainTip` method and all the methods it depends on are just copy
pasted from zcash.
## `CheckTransaction()` and `Consensus::CheckTxInputs()`
In zcash there are `ContextualCheckTransaction`, `CheckTransaction`,
`CheckTransactionWithoutProofVerification`, `Consensus::CheckTxInputs`, and
`ContextualCheckInputs` functions that coorespond to `CheckTransaction` and
`Consensus::CheckTxInputs` in the sidechain template. In zside code for checking
sapling transactions was just copy pasted. Note that there is no code for
keeping track of upgrades like sprout, overwintered, sapling, heartwood in
zside. Currently it only supports sapling.

In `CheckTransaction` we check that all shielded spends, all shielded outputs,
and the signature are valid, using functions from librustzcash library.

In `Consensus::CheckTxInputs` we call
`CCoinsViewCache::HaveShieldedRequirements` in order to check that there is no
nullifier (it is not already spent), and that the anchor is valid.

`CCoinsViewCache::HaveShieldedRequirements` is also called in validation.cpp in
`CChainState::ConnectBlock` on every transaction in the block and in
`AcceptToMemoryPoolWorker` on every transaction that goes into mempool.
## `CBlockHeader` and `CBlock` classes
- In `CBlockHeader` class `uint256 hashLightClientRoot;` is added, it is handled
  in constructors, serialized, and deserialized normally like other fields. It is
  used when handling anchors.
- In `CBlock` class vtx is an `std::vector<CTransactionRef> vtx;` (like in clean
  sidechain template) instead of `std::vector<CTransaction> vtx;` (like in
  zcash).
## `CWallet`

Here are the fields and methods that are copy pasted from zcash to sidechain
templates in `CWallet` class in wallet/wallet.h:

```c++
typedef TxSpendMap<uint256> TxNullifiers;
// TxNullifiers mapTxSproutNullifiers;
TxNullifiers mapTxSaplingNullifiers;

void AddToSpends(const COutPoint& outpoint, const uint256& wtxid);

void AddToSaplingSpends(const uint256& nullifier, const uint256& wtxid);

/* Returns the wallet's HD seed or throw JSONRPCError(...) */
HDSeed GetHDSeedForRPC() const;
std::map<uint256, SaplingOutPoint> mapSaplingNullifiersToNotes;
std::set<SaplingOutPoint> setLockedSaplingNotes;

void GetSaplingNoteWitnesses(
        std::vector<SaplingOutPoint> notes,
        std::vector<boost::optional<SaplingWitness>>& witnesses,
        uint256 &final_anchor);

bool IsSaplingSpent(const uint256& nullifier) const;
```

Methods for handling zcash keys (wallet/wallet.h:1109):

```c++
/**
    * Sapling ZKeys
    */
//! Generates new Sapling key
libzcash::SaplingPaymentAddress GenerateNewSaplingZKey();
// Needed for GenerateNewSaplingZKey
std::map<libzcash::SaplingIncomingViewingKey, CKeyMetadata> mapSaplingZKeyMetadata;

bool AddSaplingZKey(const libzcash::SaplingExtendedSpendingKey &key);

//! Adds spending key to the store, without saving it to disk (used by LoadWallet)
bool LoadSaplingZKey(const libzcash::SaplingExtendedSpendingKey &key);
//! Load spending key metadata (used by LoadWallet)
void LoadSaplingZKeyMetadata(const libzcash::SaplingIncomingViewingKey &ivk, const CKeyMetadata &meta);
//! Add Sapling full viewing key to the store, without saving it to disk (used by LoadWallet)
bool LoadSaplingFullViewingKey(const libzcash::SaplingExtendedFullViewingKey &extfvk);
//! Adds a Sapling payment address -> incoming viewing key map entry,
//! without saving it to disk (used by LoadWallet)
bool LoadSaplingPaymentAddress(
    const libzcash::SaplingPaymentAddress &addr,
    const libzcash::SaplingIncomingViewingKey &ivk);
```
  
Methods for getting filtered notes (zcash outputs), they are used to get
available funds/check if you have enough funds to spend:

```c++
/* Find notes filtered by payment address, min depth, ability to spend */
void GetFilteredNotes(//std::vector<SproutNoteEntry>& sproutEntries,
                        std::vector<SaplingNoteEntry>& saplingEntries,
                        std::string address,
                        int minDepth=1,
                        bool ignoreSpent=true,
                        bool requireSpendingKey=true);

/* Find notes filtered by payment addresses, min depth, max depth, if they are spent,
    if a spending key is required, and if they are locked */
void GetFilteredNotes(//std::vector<SproutNoteEntry>& sproutEntries,
                        std::vector<SaplingNoteEntry>& saplingEntries,
                        std::set<libzcash::PaymentAddress>& filterAddresses,
                        int minDepth=1,
                        int maxDepth=INT_MAX,
                        bool ignoreSpent=true,
                        bool requireSpendingKey=true,
                        bool ignoreLocked=true);
std::pair<mapSaplingNoteData_t, SaplingIncomingViewingKeyMap> FindMySaplingNotes(const CTransaction& tx) const;
```

Methods for handling nullifiers and NoteWitnesses, `ChainTip` method is called
right after a block is either connected or disconnected:

```c++
public:
void ChainTip(
    const CBlockIndex *pindex,
    const CBlock *pblock,
    boost::optional<SaplingMerkleTree> added);
void UpdateNullifierNoteMapWithTx(const CWalletTx& wtx);
void UpdateSaplingNullifierNoteMapForBlock(const CBlock* pblock);
void UpdateSaplingNullifierNoteMapWithTx(CWalletTx& wtx);
int64_t nWitnessCacheSize;
private:
void ChainTipAdded(const CBlockIndex *pindex, const CBlock *pblock, /* SproutMerkleTree sproutTree, */ SaplingMerkleTree saplingTree);
protected:
bool UpdatedNoteData(const CWalletTx& wtxIn, CWalletTx& wtx);
/**
 * pindex is the new tip being connected.
 */
void IncrementNoteWitnesses(const CBlockIndex* pindex,
                            const CBlock* pblock,
                            //SproutMerkleTree& sproutTree,
                            SaplingMerkleTree& saplingTree);
/**
 * pindex is the old tip being disconnected.
 */
void DecrementNoteWitnesses(const CBlockIndex* pindex);
```
## `CWalletDB`

In `CWalletDB` class there are copy pasted methods for writing zcash keys:

```c++
bool WriteSaplingZKey(const libzcash::SaplingIncomingViewingKey &ivk,
                        const libzcash::SaplingExtendedSpendingKey &key,
                        const CKeyMetadata  &keyMeta);
bool WriteSaplingPaymentAddress(const libzcash::SaplingPaymentAddress &addr,
                                const libzcash::SaplingIncomingViewingKey &ivk);
bool WriteCryptedZKey(const libzcash::SproutPaymentAddress & addr,
                        const libzcash::ReceivingKey & rk,
                        const std::vector<unsigned char>& vchCryptedSecret,
                        const CKeyMetadata &keyMeta);
bool WriteCryptedSaplingZKey(const libzcash::SaplingExtendedFullViewingKey &extfvk,
                        const std::vector<unsigned char>& vchCryptedSecret,
                        const CKeyMetadata &keyMeta);

bool WriteSaplingExtendedFullViewingKey(const libzcash::SaplingExtendedFullViewingKey &extfvk);
bool EraseSaplingExtendedFullViewingKey(const libzcash::SaplingExtendedFullViewingKey &extfvk);
```

There is also code in walletdb.cpp in ReadKeyValue method implementation for
reading keys from the DB, this code is copy pasted from zcash:
    
```c++
else if (strType == "sapzkey")
{
    libzcash::SaplingIncomingViewingKey ivk;
    ssKey >> ivk;
    libzcash::SaplingExtendedSpendingKey key;
    ssValue >> key;

    if (!pwallet->LoadSaplingZKey(key))
    {
        strErr = "Error reading wallet database: LoadSaplingZKey failed";
        return false;
    }

    //add checks for integrity
    wss.nZKeys++;
}
else if (strType == "sapextfvk")
{
    libzcash::SaplingExtendedFullViewingKey extfvk;
    ssKey >> extfvk;
    char fYes;
    ssValue >> fYes;
    if (fYes == '1') {
        pwallet->LoadSaplingFullViewingKey(extfvk);
    }

    // Viewing keys have no birthday information for now,
    // so set the wallet birthday to the beginning of time.
    pwallet->UpdateTimeFirstKey(1);
}
else if (strType == "sapzkeymeta")
{
    libzcash::SaplingIncomingViewingKey ivk;
    ssKey >> ivk;
    CKeyMetadata keyMeta;
    ssValue >> keyMeta;

    wss.nZKeyMeta++;

    pwallet->LoadSaplingZKeyMetadata(ivk, keyMeta);
}
else if (strType == "sapzaddr")
{
    libzcash::SaplingPaymentAddress addr;
    ssKey >> addr;
    libzcash::SaplingIncomingViewingKey ivk;
    ssValue >> ivk;

    wss.nSapZAddrs++;

    if (!pwallet->LoadSaplingPaymentAddress(addr, ivk))
    {
        strErr = "Error reading wallet database: LoadSaplingPaymentAddress failed";
        return false;
    }
}
```

## `CCoinsView`

In coins.h in `CCoinsView` class there are copy pasted methods:

```c++
//! Retrieve the tree (Sprout) at a particular anchored root in the chain
virtual bool GetSproutAnchorAt(const uint256 &rt, SproutMerkleTree &tree) const;

//! Retrieve the tree (Sapling) at a particular anchored root in the chain
virtual bool GetSaplingAnchorAt(const uint256 &rt, SaplingMerkleTree &tree) const;

//! Determine whether a nullifier is spent or not
virtual bool GetNullifier(const uint256 &nullifier, ShieldedType type) const;

//! Get the current "tip" or the latest anchored tree root in the chain
virtual uint256 GetBestAnchor(ShieldedType type) const;
```
    
And `CCoinsView::BatchWrite` method now also handles `hashSaplingAnchor`,
`mapSaplingAnchors`, and `mapSaplingNullifiers`. In zcash it also handles the
same fields for sprout, but in zside sprout is not supported:

```c++
//! Do a bulk modification (multiple Coin changes + BestBlock change).
//! The passed mapCoins can be modified.
virtual bool BatchWrite(CCoinsMap &mapCoins,
                        const uint256 &hashBlock,
                        const uint256 &hashSaplingAnchor,
                        CAnchorsSaplingMap &mapSaplingAnchors,
                        CNullifiersMap &mapSaplingNullifiers
                        );
```

In `CCoinsViewCache` class there are new copy pasted fields:

```c++
mutable uint256 hashSproutAnchor;
mutable uint256 hashSaplingAnchor;

mutable CAnchorsSproutMap cacheSproutAnchors;
mutable CAnchorsSaplingMap cacheSaplingAnchors;

mutable CNullifiersMap cacheSaplingNullifiers;
```
    
Analogous methods are present in txdb.h in `CCoinsViewDB` class.
## New zside RPC commands
RPC works a little bit differently in sidechain and zcash. In zcash RPC functions take `params` and `fHelp` arguments:
```c++
UniValue z_getnewaddress(const UniValue& params, bool fHelp)
```
and in sidechain they take a `request` object, that has `params` and `fHelp` as fields:
```c++
UniValue z_getnewaddress(const JSONRPCRequest& request)
```

So besides copy pasting there was a bit of mechanical work changing all `params`
to `request.params` and `fHelp` to `request.fHelp`. And in zcash `pwallet` is
global, unlike in sidechain, so this call:
```c++
CWallet * const pwallet = GetWalletForJSONRPCRequest(request);
```
was added to all RPC functions as well.

These RPC calls were added to zside from zcash:

- z_getnewaddress
- z_listaddresses
- z_exportkey
- z_importkey
- z_getbalance
- z_getoperationresult
- z_getoperationstatus
- z_sendmany
- z_viewtransaction

The most important call that actually enables sending shielded transactions is
z_sendmany, other calls are there just to set things up like generating keys and
for inspecting things.

## ZC_LoadParams
In init.cpp there is a `ZC_LoadParams` function that loads zcash params, that
were downloaded earlier, from the filesystem. It is just copy pasted.

## librustzcash
librustzcash is just copy pasted from zcash.
