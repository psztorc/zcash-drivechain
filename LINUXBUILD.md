# Build steps for Linux

# 1. Install Dependencies
    sudo apt install -y build-essential libtool autotools-dev automake \
    pkg-config bsdmainutils python3 libevent-dev libboost-all-dev \
    libssl-dev libdb-dev libdb++-dev libsodium-dev curl git


# 2. Fetch & setup zcash params (only needs to happen once per system)
cd zcash-drivechain/
zcutil/fetch-params.sh


# 3. Setup Rust (Once per system)
 curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh

 source $HOME/.cargo/env
 rustc --version  # Should output something like rustc 1.46.0 (04488afe3 2020-08-24)
 

# 4. Build
./autogen.sh

./configure --with-incompatible-bdb

# This directory should already exist but double check
mkdir .cargo

make
