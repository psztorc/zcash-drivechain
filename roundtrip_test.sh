#!/usr/bin/env bash
set -euo pipefail

ZCASH_CLI="./src/zside-cli --rpcuser=user --rpcpassword=password --rpcport=2048"
DRIVE_CLI="../mainchain/src/drivenet-cli --rpcuser=user --rpcpassword=password --rpcport=18443"

# # Sidechain address bytes
# export SIDECHAIN_ADDRESS_BYTES="0186ff51f527ffdcf2413d50bdf8fab1feb20e5f82815dad48c73cf462b8b313"
# # Sidechain build commit hash
# export SIDECHAIN_BUILD_COMMIT_HASH="84065aa5e0ab0dba138fecafa18fda13cdf8088e"
# # Sidechain build tar hash
# export SIDECHAIN_BUILD_TAR_HASH="176b7d2dddb0ec51c7989e9acf77e77cd9cf889738d17dd6b4678b27ec5c3f8e"
# ./src/drivenet-cli --rpcuser=user --rpcpassword=password --rpcport=18443 createsidechainproposal ZCASH "zcash sidechain" $SIDECHAIN_ADDRESS_BYTES 0 $SIDECHAIN_BUILD_TAR_HASH $SIDECHAIN_BUILD_COMMIT_HASH

NUM_BLOCKS=10
function mine_blocks() {
    for i in $(seq $NUM_BLOCKS)
    do
        $ZCASH_CLI refreshbmm 0.01>/dev/null
        $DRIVE_CLI generate 1 >/dev/null
    done
}

DEPOSIT_TADDR=$($ZCASH_CLI getnewaddress "" legacy)
$DRIVE_CLI createsidechaindeposit 0 $DEPOSIT_TADDR 2.0 0.01

sleep 0.5
mine_blocks

TADDR=$($ZCASH_CLI getnewaddress "" legacy)
$ZCASH_CLI sendtoaddress $TADDR 1

sleep 0.5
mine_blocks

echo "taddr balance before"
$ZCASH_CLI z_getbalance $DEPOSIT_TADDR
$ZCASH_CLI z_getbalance $TADDR

ZADDR=$($ZCASH_CLI z_getnewaddress)

$ZCASH_CLI z_sendmany $TADDR '[{"address": "'$ZADDR'" ,"amount": 0.95}]'

sleep 0.5
mine_blocks

echo "zaddr balance"
$ZCASH_CLI z_getbalance $ZADDR
# echo "taddr balance"
# $ZCASH_CLI z_getbalance $TADDR
echo $ZADDR

export DEST_TADDR=$($ZCASH_CLI getnewaddress sidechain legacy)
$ZCASH_CLI z_sendmany $ZADDR '[{"address": "'$DEST_TADDR'" ,"amount": 0.00659}]'

sleep 0.5
mine_blocks

echo $DEST_TADDR

echo "zaddr balance"
$ZCASH_CLI z_getbalance $ZADDR
echo "dest_taddr balance"
$ZCASH_CLI z_getbalance $DEST_TADDR
